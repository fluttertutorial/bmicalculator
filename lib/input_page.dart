import 'package:bmi_calculator/results_page.dart';
import 'package:bmi_calculator/round_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'BottomButton.dart';
import 'reusable_card.dart';
import 'icon_content.dart';
import 'constants.dart';
import 'calculator_brain.dart';


enum Gender { Male, Female }


class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}
  int height = 180;
  int weight = 60;
  int age = 24;
  Gender selectedGender;


class _InputPageState extends State<InputPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(child: Row(
              children: <Widget>[
                Expanded(child: ReusableCard(
                  onPress: () {
                    this.setState(() {
                      selectedGender = Gender.Male;
                    });
                  },
                    colour: selectedGender == Gender.Male ? activeCardColor : inactiveCardColour,
                    cardChild: IconContent(icon: FontAwesomeIcons.mars, label: "Male",),
                  ),
                ),


                Expanded(child: ReusableCard(
                  onPress: () {
                    this.setState(() {
                      selectedGender = Gender.Female;
                    });
                  },
                  colour: selectedGender == Gender.Female ? activeCardColor : inactiveCardColour,
                  cardChild: IconContent(icon: FontAwesomeIcons.venus, label: "Female"),
                  ),
                ),
              ],
            ),
          ),


          Expanded(
            child: ReusableCard(
              colour: activeCardColor,
              cardChild: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('HEIGHT', style: labelTextStyle,),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.alphabetic,
                      children: <Widget>[
                        Text(
                            height.toString(),
                            style: numberTextStyle
                        ),
                        Text(
                          'cm',
                          style: labelTextStyle,
                        )
                      ],
                    ),

                    SliderTheme(
                      data: SliderTheme.of(context).copyWith(
                        inactiveTrackColor: Color(0xFF8D8E98),
                        activeTrackColor: Colors.white,
                        thumbColor: Color(0xFFEB1555),
                        overlayColor: Color(0x29EB1555),
                        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15),
                        overlayShape: RoundSliderOverlayShape(overlayRadius: 30),
                      ),
                      child: Slider(
                        value: height.toDouble(),
                        min: 120.0,
                        max: 220.0,
                        onChanged: (double newValue){
                          setState(() {
                            height = newValue.round();
                          });
                        },
                      ),
                    )
                  ],
                ),
              ),
          ),


          Expanded(child: Row(
            children: <Widget>[
              Expanded(child: ReusableCard(
                colour: activeCardColor,
                cardChild: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    Text('WEIGHT', style: labelTextStyle,),
                    Text(weight.toString(), style: numberTextStyle, ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RoundIconButton(icon : FontAwesomeIcons.minus, onPressed: (){
                            setState(() {
                              weight--;
                            });
                          },
                        ),
                        RoundIconButton(icon : FontAwesomeIcons.plus, onPressed: (){
                          setState(() {
                            weight++;
                          });
                        },),
                      ],
                    ),
                  ],
                ),
                ),
              ),



              Expanded(child: ReusableCard(
                colour: activeCardColor,
                cardChild: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    Text('AGE', style: labelTextStyle,),
                    Text(age.toString(), style: numberTextStyle, ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RoundIconButton(icon : FontAwesomeIcons.minus, onPressed: (){
                          setState(() {
                            age--;
                          });
                        },),
                        RoundIconButton(icon : FontAwesomeIcons.plus, onPressed: (){
                            setState(() {
                              age++;
                            });
                        },),
                      ],
                    ),
                  ],
                ),
              ),
              ),
            ],
          )),

          BottomButton( buttonTitle: 'CALCULATE BMI', onTap: (){

            CalculatorBrain calc = CalculatorBrain(height: height, weight: weight);

            Navigator.push(context, MaterialPageRoute(builder: (context){
              return ResultsPage(
                bmiResults: calc.calculateBMI(),
                resultText: calc.getResult(),
                interpretation: calc.getInterpretation(),
              );
            }));
          },)
        ],
      )
    );
  }
}





