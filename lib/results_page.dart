import 'package:bmi_calculator/BottomButton.dart';
import 'package:bmi_calculator/calculator_brain.dart';
import 'package:bmi_calculator/reusable_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import 'constants.dart';


class ResultsPage extends StatelessWidget {

  ResultsPage({@required this.bmiResults, @required this.resultText, @required this.interpretation});

  final String bmiResults;
  final String resultText;
  final String interpretation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI Calculator'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[

          Expanded(
            child: Container(
              padding: EdgeInsets.all(15.0),
              child: Text('Your Results', style: titleTextStyle),
            ),
          ),

          Expanded(
            flex: 5,
            child: ReusableCard(
              colour: activeCardColor,
              cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(resultText, style: resultTextStyle,),

                  Text(
                    bmiResults.toUpperCase(),
                    style: bmiTextStyle,
                  ),

                  Text(
                    interpretation,
                    textAlign: TextAlign.center,
                    style: bodyTextStyle,
                  ),
                ],
              ),
            )
          ),

          BottomButton(buttonTitle: 'RE-CALCULATE', onTap: () {
            Navigator.pop(context);
          }),
        ],
      )
    );
  }
}
