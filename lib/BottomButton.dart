import 'package:flutter/material.dart';
import 'constants.dart';


class BottomButton extends StatelessWidget {

  BottomButton({@required this.buttonTitle, @required this.onTap});

  final Function onTap;
  final String buttonTitle;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Center(
            child: Text(buttonTitle, style: largeButtonStyle)
        ),
        color : bottomContainerColor,
        width: double.infinity,
        height: bottomContainerHeight,
        margin: EdgeInsets.only(top: 10.0),
      ),
    );
  }
}